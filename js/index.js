$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 3000    
    });
    $('#contactoBtn').on('show.bs.modal', function (e){
        console.log('desplegando modal');
        $('#contactoBtn').prop('disabled',true);
    });
    $('#contactoBtn').on('shown.bs.modal', function (e){
        console.log('modal desplegado');
    });
    $('#contactoBtn').on('hide.bs.modal', function (e){
        console.log('ocultando modal');
    });
    $('#contactoBtn').on('hiden.bs.modal', function (e){
        console.log('modal oculto');
        $('#contactoBtn').prop('disabled',false);
    });
});